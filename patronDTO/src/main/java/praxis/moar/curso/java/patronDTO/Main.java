package praxis.moar.curso.java.patronDTO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<Integer> listToDelete = new ArrayList<Integer>();
		List<TipoCuenta> existingData = new ArrayList<TipoCuenta>();
		List<TipoCuentaDTO> existingDataDto = new ArrayList<TipoCuentaDTO>();
		List<TipoCuenta> listToInsert = new ArrayList<TipoCuenta>();
		
		for(int i = 50; i < 60; i++ ) {
			listToInsert.add(new TipoCuenta(i, 1, "El tipo de cuenta : " + i));
			listToDelete.add(i);
		}
		
		TipoCuentaDAO tipoCuentaDAO = TipoCuentaDAO.newInstance(FactoryEnum.APACHE);

		// insert
		try {
			tipoCuentaDAO.insertAll(listToInsert);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Select con tipos a eliminar");
		try {
			existingData = tipoCuentaDAO.selectAll();
			existingData.forEach(tc -> System.out.println(tc.toString()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		existingData.forEach(tc -> existingDataDto.add((new TipoCuentaDTO(tc.getDescripcion()))));
		existingDataDto.forEach(tcDto -> System.out.println(tcDto.toString()));
		
		// update
		try {
			existingData.stream().forEach(value -> {value.setActivo(0); value.setDescripcion("Ayudaaaaa");});
			tipoCuentaDAO.updateAll(existingData);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\nSelect con actualizados");
		try {
			existingData = tipoCuentaDAO.selectAll();
			existingData.forEach(tc -> System.out.println(tc.toString()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// delete
		try {
			tipoCuentaDAO.deleteAll(listToDelete);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\nSelect sin eliminados");
		try {
			existingData = tipoCuentaDAO.selectAll();
			existingData.forEach(tc -> System.out.println(tc.toString()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
