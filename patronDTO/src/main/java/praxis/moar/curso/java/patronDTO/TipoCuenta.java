package praxis.moar.curso.java.patronDTO;

import java.io.Serializable;

/**
 * 
 * @author morarj
 *
 */
public class TipoCuenta implements Serializable {
	private static final long serialVersionUID = 1112803302609566170L;
	private String descripcion;
	private int clave;
	private int activo;
	
	public TipoCuenta(int clave, int activo, String descripcion) {
		this.descripcion = descripcion;
		this.clave = clave;
		this.activo = activo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getClave() {
		return clave;
	}
	public void setClave(int clave) {
		this.clave = clave;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	
	@Override
	public String toString() {
		return "Tipo de Cuenta: [Clave = " + this.clave + " Activo = " + this.activo + " Descripción = " + this.descripcion + "]";
	}
}
