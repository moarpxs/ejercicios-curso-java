package praxis.moar.curso.java.patronDTO;

public enum FactoryEnum {
	OLD(1),
	NEW(2),
	APACHE(3);

	private int code;
	
	private FactoryEnum(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}