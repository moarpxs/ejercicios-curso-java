package praxis.moar.curso.java.patronDTO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

/**
 * 
 * @author morarj
 *
 */
public class TipoCuentaDAOApacheImpl implements TipoCuentaDAO {
	private DataSource datasource;
	
	public TipoCuentaDAOApacheImpl() {
		BasicDataSource basicDataSource = new BasicDataSource();
		basicDataSource.setDriverClassName(DRIVER);
		basicDataSource.setUsername(DB_USER);
		basicDataSource.setPassword(DB_PASSWD);
		basicDataSource.setUrl(CONN_URL);
//		basicDataSource.setInitialSize(10);
		
		datasource = basicDataSource;
    }
	
	public List<TipoCuenta> selectAll() throws SQLException {
		List<TipoCuenta> tipoCuentas = new ArrayList<TipoCuenta>();
		
        try (Connection con = datasource.getConnection();
                PreparedStatement ps = con.prepareStatement(QUERY_SELECT);
                ResultSet rs = ps.executeQuery()) {
            
        	
        	while (rs.next()) {
            	tipoCuentas.add(new TipoCuenta(rs.getInt(COL_CUENTA_CLAVE), rs.getInt(COL_CUENTA_ACTIVO), rs.getString(COL_CUENTA_DESCRIPCION)));
            }
            
            return tipoCuentas;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
        	throw e;
        }
	}

	public void insertAll(List<TipoCuenta> tipoCuenta) throws SQLException {
		try(Connection con = datasource.getConnection()) {
			con.setAutoCommit(false);
			
			try(PreparedStatement preparedStatement = con.prepareStatement(QUERY_INSERT)) {
				for(TipoCuenta tc : tipoCuenta) {
					preparedStatement.setInt(1, tc.getClave());
					preparedStatement.setInt(2, tc.getActivo());
					preparedStatement.setString(3, tc.getDescripcion());
					
					preparedStatement.executeUpdate();
				}
				
				con.commit();
			} catch (Exception e) {
				con.rollback();
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateAll(List<TipoCuenta> tiposCuenta) throws SQLException {
		try(Connection con = datasource.getConnection()) {
			con.setAutoCommit(false);
			
			try(PreparedStatement preparedStatement = con.prepareStatement(QUERY_UPDATE)) {
				for(TipoCuenta tc : tiposCuenta) {
					preparedStatement.setInt(1, tc.getActivo());
					preparedStatement.setString(2, tc.getDescripcion());
					preparedStatement.setInt(3, tc.getClave());
					
					preparedStatement.executeUpdate();
				}
				
				con.commit();
			} catch (Exception e) {
				con.rollback();
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public void delete(int tipoCuenta) {
		
	}
	
	public void deleteAll(List<Integer> tiposCuentaId) throws SQLException {
		try(Connection con = datasource.getConnection()) {
			con.setAutoCommit(false);
			
			try(PreparedStatement preparedStatement = con.prepareStatement(QUERY_DELETE)) {
				for(Integer tc : tiposCuentaId) {
					preparedStatement.setInt(1, tc);
					
					preparedStatement.executeUpdate();
				}
				
				con.commit();
			} catch (Exception e) {
				con.rollback();
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
	}
}
