package praxis.moar.curso.java.patronDTO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author morarj
 *
 */
public class TipoCuentaDAOOldImpl implements TipoCuentaDAO {
	public TipoCuentaDAOOldImpl() {
        try {
        	Class.forName(DRIVER);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
	
	public List<TipoCuenta> selectAll() throws SQLException {
		List<TipoCuenta> tipoCuentas = new ArrayList<TipoCuenta>();
		
        try (Connection con = DriverManager.getConnection(CONN_URL, DB_USER, DB_PASSWD);
                PreparedStatement ps = con.prepareStatement(QUERY_SELECT);
                ResultSet rs = ps.executeQuery()) {
            
        	
        	while (rs.next()) {
            	tipoCuentas.add(new TipoCuenta(rs.getInt(COL_CUENTA_CLAVE), rs.getInt(COL_CUENTA_ACTIVO), rs.getString(COL_CUENTA_DESCRIPCION)));
            }
            
            return tipoCuentas;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
        	throw e;
        }
	}

	public void insertAll(List<TipoCuenta> tipoCuenta) throws SQLException {
		try(Connection con = DriverManager.getConnection(CONN_URL, DB_USER, DB_PASSWD)) {
			
			try(PreparedStatement preparedStatement = con.prepareStatement(QUERY_INSERT)) {
				for(TipoCuenta tc : tipoCuenta) {
					preparedStatement.setInt(1, tc.getClave());
					preparedStatement.setInt(2, tc.getActivo());
					preparedStatement.setString(3, tc.getDescripcion());
					
					preparedStatement.executeUpdate();
				}
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateAll(List<TipoCuenta> tiposCuenta) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public void delete(int tipoCuenta) {
		
	}
	
	public void deleteAll(List<Integer> tiposCuentaId) throws SQLException {
		try(Connection con = DriverManager.getConnection(CONN_URL, DB_USER, DB_PASSWD)) {
			try(PreparedStatement preparedStatement = con.prepareStatement(QUERY_DELETE)) {
				for(Integer tc : tiposCuentaId) {
					preparedStatement.setInt(1, tc);
					
					preparedStatement.executeUpdate();
				}
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
	}
}
