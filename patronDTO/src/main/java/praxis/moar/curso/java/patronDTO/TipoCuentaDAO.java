package praxis.moar.curso.java.patronDTO;

import java.sql.SQLException;
import java.util.List;

/**
 * 
 * @author morarj
 *
 */
public interface TipoCuentaDAO {
	String DRIVER = "oracle.jdbc.driver.OracleDriver";
	String CONN_URL = "jdbc:oracle:thin:@192.168.17.181:1521:SPEI";
	String DB_USER = "INBURSA";
	String DB_PASSWD = "hipersp31";
	String QUERY_SELECT = "select * from bmx_t_cuenta WHERE TC_CLAVE >= 50";
	String QUERY_INSERT = "INSERT INTO BMX_T_CUENTA (TC_CLAVE, TC_ACTIVO, TC_DESCRIPCION) VALUES (?, ?, ?)";
	String QUERY_UPDATE = "UPDATE BMX_T_CUENTA set TC_ACTIVO = ?, TC_DESCRIPCION = ? WHERE TC_CLAVE = ?";
    String QUERY_DELETE = "DELETE FROM BMX_T_CUENTA WHERE TC_CLAVE = ?";
	String COL_CUENTA_CLAVE = "tc_clave";
    String COL_CUENTA_DESCRIPCION = "tc_descripcion";
    String COL_CUENTA_ACTIVO = "tc_activo";
    
    public List<TipoCuenta> selectAll() throws SQLException;
    
    public void insertAll(List<TipoCuenta> tipoCuenta) throws SQLException;
    
    public void updateAll(List<TipoCuenta> tiposCuenta) throws SQLException;
    
    public void delete(int tipoCuenta) throws SQLException;
    
    public void deleteAll(List<Integer> tiposCuenta) throws SQLException;
    
    static TipoCuentaDAO newInstance(FactoryEnum type) {
    	switch(type) {
    		case OLD:
    			return new TipoCuentaDAOOldImpl();
    		case NEW:
    			return new TipoCuentaDAONewImpl();
    		case APACHE:
    		default:
    			return new TipoCuentaDAOApacheImpl();
    	}
    }
}
