package praxis.moar.curso.java.patronDTO;

/**
 * 
 * @author morarj
 *
 */
public class TipoCuentaDTO {
	private String descripcion;
	
	public TipoCuentaDTO(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return "Tipo de Cuenta DTO: [Descripción = " + this.descripcion + "]";
	}
}
