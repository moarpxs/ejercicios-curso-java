package praxis.moar.curso.java.Alumno;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

public class Alumno {
	private Long id;
	String nombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String nombreCurso;
	
	public Alumno(String nombreP, String apellidoP, String apellidoM, String nombreC) {
		nombre = nombreP;
		apellidoPaterno = apellidoP;
		apellidoMaterno = apellidoM;
		nombreCurso = nombreC;
	}
	
	public void imprimeDatos() {
		System.out.println(this.nombre + " " + this.apellidoPaterno + " " + this.apellidoMaterno + " " + this.nombreCurso);
	}
	
	public long getId() {
		return id;
	}
	
	public static void main (String[] args) {
		ArrayList<Alumno> x = new ArrayList<Alumno>();
		
		for (int i = 0; i < 20; i++)
			x.add(new Alumno("alumno" + i, "apellido paterno " + i, "apellido materno " + i, "Curso " + i));
		
		x.forEach(value -> value.imprimeDatos());
		
		System.out.println(x.size());
		
		ArrayList<Alumno> y = (ArrayList<Alumno>)x.stream().filter(p -> p.nombre.contains("a")).collect(Collectors.toList());
		y.forEach(value -> value.imprimeDatos());
		
		x.stream().limit(5).forEach((a) -> {
			a.imprimeDatos();
		});
	}
}
