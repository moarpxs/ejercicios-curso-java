package praxis.moar.curso.java.Ejercicio_Games;

import praxis.moar.curso.java.Ejercicio_Games.Playeable;

import java.util.Optional;

import praxis.moar.curso.java.Ejercicio_Games.Gameable;

public class PrincipalGame {
	String gameName;
	static double playerPosX;
	static double playerPosY;
	
	public static void main (String[] args) {
		playerPosX=5.5;
		playerPosY=6.7;
		
		Playeable playerOne = (pPosX, pPosY) -> {
			pPosX++;
			pPosY++;
			System.out.println(pPosX + " " + pPosY);
		};
		
		playerOne.walk(playerPosX, playerPosY);
		
		Gameable g = () -> {
			System.out.print("Juego");
		};
		g.startGame();
		
		Soundable s = (song) -> {
			if(Optional.ofNullable(song).equals(Optional.empty())) {
				song = "NuevoSountrack";
			}
			
			System.out.println(song);
		};
		s.playMusic("Cancion Ayudaaa");
	}
}
