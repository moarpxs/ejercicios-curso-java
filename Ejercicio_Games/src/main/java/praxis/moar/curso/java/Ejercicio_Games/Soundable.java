package praxis.moar.curso.java.Ejercicio_Games;

public interface Soundable {
	default public void setGameName(String name) {
		System.out.println(name);
    }

	void playMusic(String song);
}
