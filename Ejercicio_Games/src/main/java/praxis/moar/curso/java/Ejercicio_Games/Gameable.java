package praxis.moar.curso.java.Ejercicio_Games;

public interface Gameable {
	default void setGameName(String name) {
		System.out.println(name);
    }
	
	void startGame(); 
}
