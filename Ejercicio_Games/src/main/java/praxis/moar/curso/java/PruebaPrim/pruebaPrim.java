package praxis.moar.curso.java.PruebaPrim;

public class pruebaPrim {
	private Long id;
	private String firstName;
	private String lasName;
	private short age;
	private String email;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLasName() {
		return lasName;
	}
	public void setLasName(String lasName) {
		this.lasName = lasName;
	}
	public short getAge() {
		return age;
	}
	public void setAge(short age) {
		this.age = age;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public static void main(String args[]) {
		pruebaPrim x = new pruebaPrim();
		
		System.out.println(x.getAge());
		System.out.println(x.getEmail());
		System.out.println(x.getFirstName());
		System.out.println(x.getId());
		System.out.println(x.getLasName());
	}
}
